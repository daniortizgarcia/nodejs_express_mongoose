var mongoose = require('mongoose');

// Setup schema
var ContactSchema = new mongoose.Schema({
    name: {type: String, required: true},
    email: {type: String, required: true}
}, {timestamps: true})

// Export Contact model
mongoose.model('Contact', ContactSchema);