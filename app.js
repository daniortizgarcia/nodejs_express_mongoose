let express = require('express'),
    bodyParser = require('body-parser'),
    mongoose = require('mongoose');

// Initialize the app
let app = express();

// Configure bodyparser to handle post requests
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

mongoose.connect("mongodb://localhost:27017/classfun", { useNewUrlParser: true })

require('./models/Contact');

app.use(require('./routes'));

// finally, let's start our server...
var server = app.listen( process.env.PORT || 8080, function(){
    console.log('Listening on port ' + server.address().port);
});