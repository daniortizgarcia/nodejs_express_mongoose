var router = require('express').Router();
var mongoose = require('mongoose');
var Contact = mongoose.model('Contact');

router.get('/', function(req, res, next) {
    Contact.find().then(function(contacts){
      return res.json({contacts: contacts});
    }).catch(next);
});

module.exports = router;